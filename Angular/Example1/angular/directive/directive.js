'use strict';
angular.module('app.directives', [])
  .directive('dynamic', function () {
  
  	return {
      link: function ($rootScope, ele, attrs) {
        console.log("call directive");
      }
    };
  })
  .directive('myDomDirective', function () {
    return {
        link: function ($scope, element, attrs) {
            element.bind('click', function () {
                element.html('You clicked me!');
            });
            element.bind('mouseenter', function () {
                element.css('background-color', 'yellow');
            });
            element.bind('mouseleave', function () {
                element.css('background-color', 'white');
            });
        }
    };
})
 .directive('changeLink',function(){
 	return{
 	link:function($scope,element,attrs){
 		
 		 element.attr('href', 'https://www.google.co.in/');
 		
 	}
 };
 });
